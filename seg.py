# import numpy as np

# from scipy import ndimage as ndi
# from skimage import data
# from skimage.exposure import histogram
# from skimage.feature import canny
# from skimage.filters import sobel
# from skimage.segmentation import watershed

# coins = data.coins()
# hist, hist_centers = histogram(coins)
# edges = canny(coins/255.)
# fill_coins = ndi.binary_fill_holes(edges)

# label_objects, nb_labels = ndi.label(fill_coins)
# sizes = np.bincount(label_objects.ravel())
# mask_sizes = sizes > 20
# mask_sizes[0] = 0
# coins_cleaned = mask_sizes[label_objects]

# markers = np.zeros_like(coins)
# markers[coins < 30] = 1
# markers[coins > 150] = 2

# elevation_map = sobel(coins)

# segmentation = watershed(elevation_map, markers)
# segmentation = ndi.binary_fill_holes(segmentation - 1)
# labeled_coins, _ = ndi.label(segmentation)

# cv2.imshow('img', segmentation)
# cv2.waitKey(0)


import cv2
import numpy as np
import matplotlib.pyplot as plt

from skimage.util import img_as_float
from skimage.color import rgb2gray
from skimage.filters import sobel
from skimage.segmentation import felzenszwalb, slic, quickshift, watershed, mark_boundaries, chan_vese

path = 'dataset/images/grilled_salmon/51175.jpg'


# img = img_as_float(cv2.imread(path))

# segments_fz = felzenszwalb(img, scale=100, sigma=0.5, min_size=50)
# segments_slic = slic(img, n_segments=10, compactness=10, sigma=1)
# segments_quick = quickshift(img, kernel_size=3, max_dist=6, ratio=0.5)
# segments_watershed = watershed(sobel(rgb2gray(img)), markers=25, compactness=0.0001)

# print(f"Felzenszwalb number of segments: {len(np.unique(segments_fz))}")
# print(f"SLIC number of segments: {len(np.unique(segments_slic))}")
# print(f"Quickshift number of segments: {len(np.unique(segments_quick))}")

# fig, ax = plt.subplots(2, 2, sharex=True, sharey=True)

# ax[0, 0].imshow(mark_boundaries(img, segments_fz))
# ax[0, 0].set_title("Felzenszwalbs's method")

# ax[0, 1].imshow(mark_boundaries(img, segments_slic))
# ax[0, 1].set_title('SLIC')

# ax[1, 0].imshow(mark_boundaries(img, segments_quick))
# ax[1, 0].set_title('Quickshift')

# ax[1, 1].imshow(mark_boundaries(img, segments_watershed))
# ax[1, 1].set_title('Compact watershed')

# for a in ax.ravel():
#     a.set_axis_off()

# plt.tight_layout()
# plt.show()




image = img_as_float(cv2.cvtColor(cv2.imread(path, 1), cv2.COLOR_BGR2GRAY))
cv = chan_vese(image, mu=0.25, lambda1=1, lambda2=1, tol=1e-3, max_iter=200, dt=0.5, init_level_set="checkerboard", extended_output=True)

fig, axes = plt.subplots(2, 2)
ax = axes.flatten()

ax[0].imshow(image, cmap="gray")
ax[0].set_axis_off()
ax[0].set_title("Original Image")

ax[1].imshow(cv[0], cmap="gray")
ax[1].set_axis_off()
ax[1].set_title("Chan-Vese segmentation - {} iterations".format(len(cv[2])))

ax[2].imshow(cv[1], cmap="gray")
ax[2].set_axis_off()
ax[2].set_title("Final Level Set")

ax[3].set_axis_off()
plt.show()


print(image.shape)

image = cv2.imread(path)
im_bw = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)

blur = cv2.GaussianBlur(im_bw, (5,5), 0)
im_bw = cv2.Canny(blur, 20, 120)
cv2.imshow('canny', im_bw)

contours, hierarchy = cv2.findContours(im_bw, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
cv2.drawContours(image, contours, -1, (0,255,0), 2)
cv2.imshow('img', image)

print(len(contours))
cv2.waitKey(0)
