import cv2
import numpy as np
from PIL import ImageTk 
from PIL import Image as ImgX
from food import *
from tkinter import *
from tkinter import filedialog
from skimage import exposure, feature
from skimage.filters import threshold_yen
from skimage.util import img_as_float
from skimage.segmentation import chan_vese


def insert():
    print(name_field.get())
    name_field.focus_set() 
    name_field.delete(0, END) 

def updateImg(img):
    try:
        image = ImageTk.PhotoImage(ImgX.fromarray(img))
        panel.configure(image=image)
        panel.image = image
    except Exception as e: # work on python 3.x
        print(str(e))

def loadFile():
    global img
    path = filedialog.askopenfilename(filetypes = (("JPG files", "*.jpg"),("All files", "*.*")))
    img = cv2.cvtColor(cv2.imread(path), cv2.COLOR_BGR2RGBA)
    updateImg(img)

def resizeGui():
    global img
    img = resize(img)
    updateImg(img)

def removeNoiseGui():
    global img
    img = removeNoise(img)
    updateImg(img)


def lbpGui():
    global img
    hist, img2 = LBP(img)
    # showBarChart('lbp', hist)
    yen_threshold = threshold_yen(img2)
    img2 = exposure.rescale_intensity(img2, (0, yen_threshold), (0, 255))
    updateImg(img2)

def clbpGui():
    global img
    img2 = Clbp(img)
    yen_threshold = threshold_yen(img2)
    img2 = exposure.rescale_intensity(img2, (0, yen_threshold), (0, 255))
    updateImg(img2)

def hogGui():
    global img
    x, img2 = hog(img)
    yen_threshold = threshold_yen(img2)
    img2 = exposure.rescale_intensity(img2, (0, yen_threshold), (0, 255))
    updateImg(img2)

def gaborGui():
    global img
    img2 = gabor(img)
    print(img2.shape)
    updateImg(img2)

def haarGui():
    global img
    x, y  = haarlike(img)
    print(x.shape)
    print(y.shape)
    updateImg(y)
    # feature.draw

def segmen():
    global img
    image = img_as_float(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY))
    cv = chan_vese(image, mu=0.25, lambda1=1, lambda2=1, tol=1e-3, max_iter=200, dt=0.5, init_level_set="checkerboard", extended_output=True)
    img2 = cv[0]
    yen_threshold = threshold_yen(cv[1])
    img2 = exposure.rescale_intensity(cv[1], (0, yen_threshold), (0, 255))
    updateImg(img2)



root = Tk()
root.maxsize(900, 600) 

left_frame = Frame(root)
left_frame.pack(side='left', fill='both', padx=10, pady=5)

right_frame = Frame(root)
right_frame.pack(side='right', fill='both', padx=10, pady=5, expand=True)

# Label(left_frame, text="Name").pack()
# Frame(left_frame, height=1, width=200, bg='black').pack()

# name_field = Entry(left_frame)
# name_field.pack()

Button(left_frame, text="1 - load", command=loadFile).pack()
Button(left_frame, text="2 - resize", command=resizeGui).pack()
Button(left_frame, text="3 - remove noise", command=removeNoiseGui).pack()
Label(left_frame, text="______________").pack()
Button(left_frame, text="lbp", command=lbpGui).pack()
Button(left_frame, text="clbp", command=clbpGui).pack()
Button(left_frame, text="hog", command=hogGui).pack()
Button(left_frame, text="gabor", command=gaborGui).pack()
Button(left_frame, text="haar", command=haarGui).pack()
Button(left_frame, text="segmentation", command=segmen).pack()


panel = Label(right_frame)
panel.pack()
img = None
updateImg(img)

root.mainloop()
