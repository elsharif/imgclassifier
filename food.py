#
# lbp  :  numPoints=24, radius=8, eps=1e-7
# hog  :  orientations=16, pixels_per_cell=(16, 16), cells_per_block=(1, 1)
# gabor:  orientations=4, ksize:31, sigma:1.0, lambd:15, gamma:0.02, psi:0,
# haar :  shrinking_percent=0.1
# clbp :  radius=1, numPixels=2
#
#

import cv2
import os, glob
import numpy as np
from imutils import paths
from scipy import ndimage as ndi
from matplotlib import pyplot as plt
from skimage import feature, exposure, transform
from skimage.filters import gabor_kernel
from sklearn.cluster import MeanShift, estimate_bandwidth
from clbp import genLocalPatterns


def showBarChart(title, data):
    fig = plt.figure(title)
    ax = fig.add_axes([0,0,1,1])
    ax.set_ylabel('percentage')
    ax.set_title(title)
    ax.set_xticks(np.arange(0, 81, 10))

    labels = []
    for t in range(len(data)):
        labels.append(t)
    ax.bar(labels, data)
    plt.show(block=False)

def resize(img, width=200, height=200):
    return cv2.resize(img, (int(width), int(height)), interpolation = cv2.INTER_AREA)

def removeNoise(img, noiseFactor=5):
    if noiseFactor % 2 == 0:
        raise Exception('"noiseFactor" from "removeNoise" should be odd number')
    median = cv2.medianBlur(img, noiseFactor) # high-frequency noise removal
    nosieMatrix = np.ones((noiseFactor,noiseFactor),np.float32)/(noiseFactor**2)
    return cv2.filter2D(median, -1, nosieMatrix) # low-frequency noise removal

def LBP(image, numPoints=24, radius=8, eps=1e-7):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    lbp = feature.local_binary_pattern(gray, numPoints, radius, method="uniform")
    hist, _ = np.histogram(lbp.ravel(), bins=np.arange(0, numPoints + 3), range=(0, numPoints + 2))
    hist = hist.astype("float")
    hist /= (hist.sum() + eps) # normalize the histogram
    return hist, lbp

def hog(image):
    fd, hog_image = feature.hog(image, orientations=16, pixels_per_cell=(16, 16), cells_per_block=(1, 1), visualize=True, multichannel=True)
    return fd, hog_image

def gabor(img, orientations=4):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    filters = []
    ksize = 31
    for theta in np.arange(0, np.pi, np.pi / orientations):
        params = {'ksize':(ksize, ksize), 'sigma':1.0, 'theta':theta, 'lambd':15.0, 'gamma':0.02, 'psi':0, 'ktype':cv2.CV_32F}
        kern = cv2.getGaborKernel(**params)
        kern /= 1.5 * kern.sum()
        filters.append((kern, params))
    accum = np.zeros_like(img)
    for kern, params in filters:
        fimg = cv2.filter2D(img, cv2.CV_8UC3, kern)
        np.maximum(accum, fimg, accum)
    return accum


def haarlike(img, shrinking_percent=0.1):
    img = resize(img, img.shape[0] * shrinking_percent, img.shape[1] * shrinking_percent)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    intimg = transform.integral_image(gray)
    feature_types = ['type-2-x', 'type-2-y']
    res = feature.haar_like_feature(intimg, 0, 0, intimg.shape[0], intimg.shape[1], feature_type=feature_types)

    feature_coord, _ = feature.haar_like_feature_coord(intimg.shape[0], intimg.shape[1],  feature_types)
    haarimg = feature.draw_haar_like_feature(intimg, 0, 0, intimg.shape[0], intimg.shape[1], feature_coord, max_n_features=1)
    
    return res, haarimg

def Clbp(img, radius=1, numPixels=2):
    mapping = 'clbp_m_riu2' # options: lbp, clbp_s, clbp_s_riu2, clbp_m, clbp_m_riu2, clbp_c
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    c = genLocalPatterns(gray, radius, numPixels, mapping)
    c = np.array(c,dtype=np.uint8)
    return c

def extract(path):
    img = cv2.imread(path, 1)
    img = resize(img)
    img = removeNoise(img)
    
    lbp, imgLBP = LBP(img, 24, 8)
    g = gabor(img)
    h, himg = hog(img)
    haar, _ = haarlike(img)
    c = '' # Clbp(img)

    return lbp, g, h, haar, c

def getAllImages(datasetpath, outputpath, action):
    for root, dirs, files in os.walk(datasetpath):
        dirs.sort()
        files.sort()
        for file in files:
            if file.endswith('.jpg'):
                terminate = action(root.split('/')[-1], file, root +'/'+ file, outputpath)
                if terminate == True:
                    return

def ats(array):
    return ','.join([str(x) for x in array])
def flat(matrix):
    return np.array(matrix).ravel() 

counter = 0
def iterateFilesAction(folder, filename, fullpath, outputpath):
    global counter
    counter += 1
    print(f'{counter} -  {folder} - {filename}')
    
    lbp, gabor, hog, haar, clbp = extract(fullpath)

    postfix = f',{filename},{folder}'

    writeline(f'{outputpath}lbp.csv', f'{ats(lbp)}{postfix}')
    writeline(f'{outputpath}hog.csv', f'{ats(hog)}{postfix}')
    writeline(f'{outputpath}gabor.csv', f'{ats(flat(gabor))}{postfix}')
    writeline(f'{outputpath}haar.csv', f'{ats(haar)}{postfix}')
    # writeline(f'{outputpath}clbp.csv', f'{ats(clbp)}{postfix}')
    
    # if you want to stop the iterator just return True
    return False

def writeline(path, txt):
    with open(path, "a+") as file:
        file.seek(0)
        if len(file.read(100)) > 0:
            file.write("\n")
        file.write(txt)

if __name__ == "__main__":
    getAllImages(datasetpath='dataset/images/', outputpath='output/', action=iterateFilesAction)
